package upload

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	artifactregistry "cloud.google.com/go/artifactregistry/apiv1"
	"cloud.google.com/go/artifactregistry/apiv1/artifactregistrypb"
	"github.com/google/go-containerregistry/pkg/crane"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
)

const (
	endpoint        = "https://artifactregistry.googleapis.com/v1beta2"
	ArAnnotationKey = "console.cloud.google.com/external_link"
	userAgent       = "google-gitlab-components:artifact-registry-upload"
)

func PullAndUploadImage(source, target string) error {
	img, err := crane.Pull(source)
	if err != nil {
		return err
	}

	ctx := context.Background()
	client, err := google.DefaultClient(ctx, "https://www.googleapis.com/auth/cloud-platform")
	if err != nil {
		return err
	}

	return crane.Push(img, target, crane.WithTransport(client.Transport), crane.WithUserAgent(userAgent))
}

func ImgPath(target string) string {
	splitted := strings.SplitN(target, "/", 4)
	hostname, project, repo, imageWithTag := splitted[0], splitted[1], splitted[2], splitted[3]
	location := strings.TrimSuffix(hostname, "-docker.pkg.dev")
	splitted = strings.Split(imageWithTag, ":")
	image := splitted[0]
	path := fmt.Sprintf("projects/%s/locations/%s/repositories/%s/packages/%s", project, location, repo, image)
	return path
}

func UpdateARMetadata(req *http.Request, client HTTPClient) (*ARPackageMetadata, error) {
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	// Process the response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return nil, err
	}
	defer resp.Body.Close()
	metadata := ARPackageMetadata{}
	err = json.Unmarshal(body, &metadata)
	if err != nil {
		fmt.Println("Error unmarshalling metadata:", err)
		return nil, err
	}
	return &metadata, nil
}

type ARPackageMetadata struct {
	Name        string            `json:"name"`
	Annotations map[string]string `json:"annotations"`
}

func ARUpdateRequest(link, path, url string) (*http.Request, error) {
	fmt.Println("requesting ", url)
	payload := ARPackageMetadata{
		Name: path,
		Annotations: map[string]string{
			ArAnnotationKey: link,
		},
	}
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error creating JSON:", err)
		return nil, err
	}
	req, err := http.NewRequest("PATCH", url, bytes.NewBuffer(payloadBytes))
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", userAgent)
	return req, nil
}

func GetPackage(path string) error {
	ctx := context.Background()
	c, err := artifactregistry.NewClient(ctx, option.WithUserAgent(userAgent))
	if err != nil {
		return err
	}
	defer c.Close()
	p, err := c.GetPackage(ctx, &artifactregistrypb.GetPackageRequest{Name: path})
	if err != nil {
		return err
	}
	fmt.Println("package name", p.Name)
	return nil
}

type Repository struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Path      string `json:"path"`
	Location  string `json:"location"`
	CreatedAt string `json:"created_at"`
}

type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// / GetLink retrieves the GitLab Registry Container link for a given image source.
func GetLink(source, apiURL string, client HTTPClient) (string, error) {
	// Create the HTTP request
	req, err := http.NewRequest("GET", apiURL, nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return "", err
	}

	// Execute the request
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making request:", err)
		return "", err
	}
	defer resp.Body.Close()
	var repositories []Repository // Define a struct to match the JSON structure

	if err := json.NewDecoder(resp.Body).Decode(&repositories); err != nil {
		fmt.Println("Error decoding JSON:", err)
		return "", err
	}
	link := ""

	// Now you have a slice of 'Repository' structs
	splitted := strings.Split(source, ":")

	for _, repo := range repositories {
		if repo.Location == splitted[0] {
			proj := strings.TrimSuffix(repo.Path, repo.Name)
			link = fmt.Sprintf("https://gitlab.com/%scontainer_registry/%d", proj, repo.ID)
			fmt.Println("The url of image in GitLab Registry", link)
		}
	}
	return link, nil
}
