package upload_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab/yongxuanzhang/gar-upload-go-binary/pkg/upload"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPullAndUploadImage(t *testing.T) {
	source := "registry.gitlab.com/yongxuanzhang/myapp/auth-with-token:latest"
	target := "us-central1-docker.pkg.dev/gitlab-zhangquan/quickstart-docker-repo/auth-with-token-new:tag1"
	err := upload.PullAndUploadImage(source, target)
	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestImgPath(t *testing.T) {
	imgPathTests := []struct {
		name     string
		target   string
		expected string
	}{
		{
			name:     "Standard Input",
			target:   "us-central1-docker.pkg.dev/project/repo/image:imagetag",
			expected: "projects/project/locations/us-central1/repositories/repo/packages/image"},
		{
			name:     "No image tag",
			target:   "us-central1-docker.pkg.dev/project/repo/image",
			expected: "projects/project/locations/us-central1/repositories/repo/packages/image"}, // Should probably return an error, but assuming current logic
		{
			name:     "image name contains `/`",
			target:   "us-central1-docker.pkg.dev/project/repo/image/test",
			expected: "projects/project/locations/us-central1/repositories/repo/packages/image/test"},
	}
	for _, tt := range imgPathTests {
		t.Run(tt.name, func(t *testing.T) {
			result := upload.ImgPath(tt.target)
			if result != tt.expected {
				t.Errorf("ImgPath(%q) failed. Expected: %q, Got: %q", tt.target, tt.expected, result)
			}
		})
	}
}

func TestUpdateARMetadata(t *testing.T) {
	path := "imgPath"
	endpoint := "http://artifactregistry.googleapis.com/v1beta2"
	gitlabUrl := "https://gitlab.com/user/app/container_registry/111]"

	req, err := upload.ARUpdateRequest(gitlabUrl, path, fmt.Sprintf("%s/%s?update_mask=annotations", endpoint, path))
	if err != nil {
		t.Error(err)
	}

	metadata := upload.ARPackageMetadata{
		Name: path,
		Annotations: map[string]string{
			upload.ArAnnotationKey: gitlabUrl,
		},
	}

	resp, err := json.Marshal(metadata)
	if err != nil {
		t.Error(err)
	}

	// Create a test server
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(resp)
	}))
	defer ts.Close()

	// Create a custom HTTP client that points to the test server
	mockClient := &http.Client{
		Transport: &http.Transport{
			Proxy: func(req *http.Request) (*url.URL, error) {
				return url.Parse(ts.URL)
			},
		},
	}

	got, err := upload.UpdateARMetadata(req, mockClient)
	if err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(got, &metadata); diff != "" {
		t.Error(diff)
	}
}

func TestARUpdateRequest_Success(t *testing.T) {
	// Input values
	link := "test-link"
	path := "test-path"
	url := "http://test.ar.com/update"

	// Execute the function
	req, err := upload.ARUpdateRequest(link, path, url)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	// 1. Assert HTTP Method
	if req.Method != "PATCH" {
		t.Errorf("Incorrect method, expected PATCH, got %s", req.Method)
	}

	// 2. Assert URL
	if req.URL.String() != url {
		t.Errorf("Incorrect URL, expected %s, got %s", url, req.URL.String())
	}

	// 3. Assert Payload
	expectedPayload := `{"name":"test-path","annotations":{"console.cloud.google.com/external_link":"test-link"}}` // Define as per your structure and ArAnnotationKey
	buf := new(bytes.Buffer)
	buf.ReadFrom(req.Body)
	if buf.String() != expectedPayload {
		t.Errorf("Incorrect payload. Expected: %s, Got: %s", expectedPayload, buf.String())
	}

	// 4. Assert headers
	if req.Header.Get("User-Agent") != "google-gitlab-components:artifact-registry-upload" {
		t.Error("Incorrect User-Agent header")
	}
}

func TestGetLink(t *testing.T) {
	repositories := []upload.Repository{{
		ID:       1,
		Name:     "",
		Path:     "group/project/",
		Location: "registry.gitlab.com/my-group/test-repo",
	}}

	resp, err := json.Marshal(repositories)
	if err != nil {
		t.Error(err)
	}

	// Create a test server
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(resp)
	}))
	defer ts.Close()

	// Create a custom HTTP client that points to the test server
	mockClient := &http.Client{
		Transport: &http.Transport{
			Proxy: func(req *http.Request) (*url.URL, error) {
				return url.Parse(ts.URL)
			},
		},
	}
	apiURL := "http://gitlab.com/api/v4/projects/test-project/registry/repositories?job_token=test-token"
	// Call your function with your source string and mock client
	link, err := upload.GetLink("registry.gitlab.com/my-group/test-repo:latest", apiURL, mockClient)
	if err != nil {
		t.Fatal(err)
	}

	expectedLink := fmt.Sprintf("https://gitlab.com/%scontainer_registry/%d", repositories[0].Path, repositories[0].ID)
	if link != expectedLink {
		t.Errorf("Expected link %s, got %s", expectedLink, link)
	}
}
