/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"fmt"
	"gitlab/yongxuanzhang/gar-upload-go-binary/pkg/upload"
	"log"
	"net/http"
	"os"

	"github.com/spf13/cobra"
	"golang.org/x/oauth2/google"
)

var (
	source     string
	target     string
	gitlabLink string
)

const (
	endpoint = "https://artifactregistry.googleapis.com/v1beta2"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gar-upload",
	Short: "gar-upload is used to upload docker image to Google Artifact Registry",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		logger := log.Default()
		// copy image
		err := upload.PullAndUploadImage(source, target)
		if err != nil {
			return err
		}
		log.Println("Image pushed to AR")

		// Construct the API URL
		projectID := os.Getenv("CI_PROJECT_ID")
		jobToken := os.Getenv("CI_JOB_TOKEN")
		apiURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/registry/repositories?job_token=%s", projectID, jobToken)
		link, err := upload.GetLink(source, apiURL, &http.Client{})
		if err != nil {
			logger.Println("get error when getting container url:", err)
		}

		// update package metadata
		path := upload.ImgPath(target)
		req, err := upload.ARUpdateRequest(link, path, fmt.Sprintf("%s/%s?update_mask=annotations", endpoint, path))
		if err != nil {
			logger.Println("get error when preparing request:", err)
		}
		clientWithAuth, err := google.DefaultClient(context.Background(), "https://www.googleapis.com/auth/cloud-platform")
		if err != nil {
			logger.Println("get error when getting client with credentials:", err)
		}
		metadata, err := upload.UpdateARMetadata(req, clientWithAuth)
		if err != nil {
			logger.Println("get error when updating package metadata:", err)
		}
		if metadata.Annotations[upload.ArAnnotationKey] != link {
			logger.Printf("fail to update cross link, want: %s got: %s", link, metadata.Annotations[upload.ArAnnotationKey])
		} else {
			logger.Println("successfully update package metadata:", metadata)
		}
		// confirm package
		err = upload.GetPackage(path)
		if err != nil {
			logger.Println("get error when getting package:", err)
		}
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd.PersistentFlags().StringVarP(&source, "source", "", "", "Source Image Path")
	rootCmd.PersistentFlags().StringVarP(&target, "target", "", "", "Target Image Path")
	rootCmd.PersistentFlags().StringVarP(&gitlabLink, "gitlab-registry-repo", "", "", "gitlab registry repo")
}
