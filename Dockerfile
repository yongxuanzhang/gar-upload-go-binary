FROM zyx1200/google-cloud-auth as auth-base

FROM golang:1.19-alpine as builder
WORKDIR /app
COPY . .
RUN go build -o gar-upload

# Start a new stage to create a smaller image
FROM alpine:3.14
WORKDIR /app
RUN apk update && apk add bash curl
COPY --from=builder /app/gar-upload /usr/bin/gar-upload
COPY --from=auth-base /usr/bin/google-cloud-auth /usr/bin/google-cloud-auth
CMD ["gar-upload"]
